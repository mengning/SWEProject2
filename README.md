Website Link: http://spotifynder.com

# SWEProject2
Group 18


| Name | EID | GitLab ID |
| ------ | ------ | ------|
| Andrew Li | al48859 | andrewli6504 |
| Nathaniel Jackson | nlj428 | nathanljackson112 |
| Connor Kite | cjk2628 | ckite2k |
| Mengning Geng | xg2759 | mengning |
| Blake Romero | bar3394 | BlakeRomero |

GitLab SHA (last commit for Phase IV): 2957485228238b8d012459b9cdd014c8410903d2

GitLab SHA (last commit with visualizations and presentation): a5265a5a96d272a34e3c3f95dd3898b3c33a8f62

| Name | Estimated Completion Time (hours) |
| ------ | ------ |
| Andrew | 20 |
| Nathan | 20 |
| Connor | 20 |
| Mengning | 20 |
| Blake | 20 |

| Name | Actual Completion Time (hours) |
| ------ | ------ |
| Andrew | 25 |
| Nathan | 25 |
| Connor | 20 |
| Mengning | 20 |
| Blake | 25 |


Presentation: https://www.youtube.com/watch?v=sfkRZMK8e7U

Gitlab Repo: https://gitlab.com/ckite2k/SWEProject2

Postman API: https://documenter.getpostman.com/view/14731082/Tz5jefMk

Technical Report: https://docs.google.com/document/d/1uTNTXvUlgkGXhpA5s114WayK8ZAEgr4UmsvzrmuPUAM/edit?usp=sharing

Phase IV Leader: Andrew Li

# Spotifynder Start Up Guide

The following is a guide to start developing on the Spotifynder project. If you come across an issue that is not in this guide, send a message in the Discord and it will get added shortly.


# First Steps

To start development, you'll need to clone this Git repo.

## Requirements

You'll need to have Git installed locally and an IDE of your choosing (personally prefer VS  Code for its Git interface and built-in terminal).

You will also need to install npm (node package manager) and python (version 3.9.2) if you have not already done so.

(Note: python seems to be especially tricky if you have multiple versions downloaded. If you have python 3.x.x installed locally, see if you can follow the rest of the steps without having to install and handle multiple versions).

## git clone

Run the following command to clone the repo:

$ `git clone git@gitlab.com:ckite2k/SWEProject2.git`

## Installing local dependencies

Spotifynder works off of several dependencies. To keep the implementation the same across each developer, these dependencies are recreated in docker images when running the frontend and backend servers with docker-compose (see section 'Docker-Compose').

However, it may aid your development to have the node modules installed locally.

### Front-End

For frontend development, this can be done by navigating to src/frontend, then running the following command:

$ `npm install`

This will install all of the dependencies locally in a node_modules folder, within frontend. Ensure that you don't commit the node_modules folder to the gitlab repo! This shouldn't be done unless you modify the .gitignore file within frontend.

### Back-End
The process is slightly different for backend. You will have to have python installed locally (this project works on python 3.9.2). 

Then, navigate to src/backend, and run the following command:

$ `python -m pip install -r requirements.txt`

This should install all of the local dependencies you need to do back-end development (implementing and testing API).


# Running the Web App Locally

Our web app runs off of front-end and back-end servers, which are encapsulated within docker images. This is to ensure that each developer is working off of the same dependencies throughout the project.

Follow the proceeding steps to run the web app on your local machine.

## Create a Docker Hub account

Create a Docker Hub account at the following address:

https://hub.docker.com/signup

You will need your login credentials to run docker and docker-compose locally!

(Can't entirely remember how the docker set up works step for step. Would someone ping me in discord once they set up docker and lmk how and when credentials are needed?)

## Install Docker and Docker-Compose

Install both Docker and Docker-Compose via npm. Use the following commands to do so globally:

$ `npm install -g docker`

$ `npm install --save-dev docker-compose`

## How Docker-Compose Works

All of the heavy lifting with Docker has been done for you! Here is how it works, if you are interested:

1.  Docker images are made from Dockerfiles. These have a list of commands that are executed in order.
2. Each Dockerfile (one in src/frontend, one in src/backend) essentially do the same thing.
	* Copies a pre-built image from online 
		* node for frontend, python for backend
	* Creates a folder within the image and specifies that it should be the working directory
	* Copies the package.json over to that folder 
		* This file contains all dependencies the image will have.
	* Copies the rest of the files in the current directory to the image's working directory
	* Installs each of the dependencies
	* Specifies a port to run on
		* Port 80 for Frontend, Port 5000 for Backend
	* Runs a script within package.json that launches the server (frontend or backend)
3. To reduce the amount of commands you have to run, docker-compose allows you to launch both of the dockerfiles simultaneously.

## How to Run Docker-Compose

Assuming you have already logged in with your Docker login credentials, navigate to the root directory (the directory in which the whole project is contained).

Run the following command to run docker-compose and launch both the frontend and backend servers:

$ `docker-compose up --build -d`

The --build flag will rebuild images each time you launch your servers.

To ensure your docker images are running, run the following command:

$ `docker ps`

You should see something like the following if your images have launched properly:

`CONTAINER ID   IMAGE                  COMMAND                  CREATED         STATUS         PORTS                    NAMES`

`5949d1257bcf   spotifynder_backend    "flask run --host=0.…"   7 seconds ago   Up 4 seconds   0.0.0.0:5000->5000/tcp   backend`

`bb344a7a7ae8   spotifynder_frontend   "docker-entrypoint.s…"   7 seconds ago   Up 5 seconds   0.0.0.0:80->80/tcp       frontend`

## How to Stop Docker-Compose

To shut down your Docker server, run the following command:

$ `docker-compose down`	

# Git Branching
There are three main branches within the Spotifynder GitLab repository: `master`, `stage`, and `development`.

## Branching Guarantees

The Spotifynder project works with Git branches to ensure the following:
1. The `master` branch always has a stable version of the website. 
	* This will always be the version of the last phase checkpoint, until that phase has been graded.
	
2. The `stage` branch always has a stable version of the website. 
	* This branch will typically be more up-to-date than the master branch, as this will merge into master once the previous phase has been graded.
3. The `development` branch will be the version in which current developments are being added to.
	* This will typically be stable, unless a recent merge-request crashes the CI/CD pipeline (to be set up in Phase 2).
	
	* All development will occur locally on branches stemming from `development`, merging to `development` when done. See the 'Development Branching' section for more information.

## Git Commands for Branching

### Create a Branch on GitLab

To create a new branch, I would suggest doing so from the GitLab repository to avoid confusion.
1. Navigate to Repository > Branches.
2. Click on the 'New Branch' button.
3. Title your branch name.
	* Each branch should only do one thing. Use a descriptive title to convey this one thing your branch accomplishes.
4. IMPORTANT: change the 'Create from' tab from `master	` to `development`.
	* This ensures that you are always working off of the latest version of `development`.
5. Your branch is now created!

### Checkout to your new Branch Locally

To access your branch locally, first checkout to the `development` branch.

You will need to fetch it from the repository with the following command:

$ `git fetch`

You should see a message like the following if fetching has worked correctly:

`From https://gitlab.com/ckite2k/SWEProject2`
` * [new branch]      branch-name-here -> origin/branch-name-here`

Fetching will automatically occur if you call $ `git pull` as well.

Then, checkout to that branch with the following command:

$ `git checkout branch-name-here`

You should be developing on your new branch! Any commits you push will be pushed to the remote version of your branch.

## Merging your Branch to Development

When you have made the desired developments for a particular branch (and implemented tests for it, Phase 2 and beyond), merge your branch back into `development`!

### Create Merge Request

On GitLab, follow these steps to create a new merge request:

1. Navigate to Repository > Branches.
2. Click the 'Merge Request' button for the branch you would like to merge.
3. IMPORTANT: change the branch you are merging to from `master` to `development`.
	* This is extremely important! We do not want to merge directly to master and potentially violate our guarantee of a stable build on the `master` branch.
4. Write an apt description of what your code does and what problem you are attempting to solve.
5. Assign the merge request to yourself and add relevant tags.
6. Submit your merge request.
7. Inform @everyone in the Discord channel that you have a new merge request.
	* By rule, each merge request will require two additional approvals. 
	* This is only to ensure you are merging to the correct repository and that people are informed of new changes, especially if they will merge conflict with other code.
8. Once your merge request has been approved by two others, merge your code to the `development` branch.

Congrats! Now your implemented code is the `development` branch. To work on more code, make a new branch from `development` and follow the steps again.

### Pulling New Code

When updates are made to `development` (or `stage` and `master`, on occasion), you will need to update your local branches to reflect those changes.

This is accomplished by first checking out to the relevant branch:

`git checkout <branch-name>`

Then, run the following command to fetch and merge any new updates:

`git pull`

You may run into merge conflicts, where new code from `development` may not be compatible with your code. You will need to resolve these conflicts in such a way that the new code is preserved.

If you are working on a branch, you will need to checkout to that branch and merge to get the updates from `development`. 

`git merge origin/development` for getting updates from remote `development` branch, for instance.

This should be done after you checkout to `development` and have pulled the updates.


>>>>>>> README.md
