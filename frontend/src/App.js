import React from "react"
import { Route, Switch } from "react-router-dom"

//import custom bootstrap theme
import "./custom-theme.scss"

//import pages here
import HomePage from "./components/Pages/Home/HomePage"
import SongsPage from "./components/Pages/Songs/SongsPage"
import SongInstancePage from "./components/Pages/Songs/SongInstancePage"
import ArtistsPage from "./components/Pages/Artists/ArtistsPage"
import AlbumsPage from "./components/Pages/Albums/AlbumsPage"
import AlbumInstance from "./components/Pages/Albums/AlbumInstance"
import AboutPage from "./components/Pages/About/AboutPage"
import ErrorPage from "./components/Pages/Error/ErrorPage"
import ArtistInstancePage from "./components/Pages/Artists/ArtistInstancePage"
import GlobalSearchPage from "./components/Pages/GlobalSearchPage"
import SongSearchPage from "./components/Pages/Songs/SongSearchPage"
import ArtistSearchPage from "./components/Pages/Artists/ArtistSearchPage"
import SongComparePage from "./components/Pages/Songs/SongComparePage"
import ArtistComparePage from "./components/Pages/Artists/ArtistComparePage"
import AlbumComparePage from "./components/Pages/Albums/AlbumComparePage"
import Visualizations from "./components/Pages/Visualizations/Visualizations"

function App() {
	return (
		<main>
			<Switch>
				<Route path="/" component={HomePage} exact />

				<Route exact path="/songs" component={SongsPage} />
				<Route path="/song/:songID" component={SongInstancePage} />
				<Route path="/songs/search" component={SongSearchPage} />
				<Route path="/songs/compare/:song1ID/:song2ID" component={SongComparePage} />

				<Route exact path="/artists" component={ArtistsPage} />
				<Route path="/artist/:artistID" component={ArtistInstancePage} />
				<Route path="/artists/search" component={ArtistSearchPage} />
				<Route path="/artists/compare/:artist1ID/:artist2ID" component={ArtistComparePage} />

				<Route exact path="/albums" component={AlbumsPage} />
				<Route path="/album/:albumID" component={AlbumInstance} />
				<Route path="/albums/compare/:album1ID/:album2ID" component={AlbumComparePage} />

				<Route path="/about" component={AboutPage} />

				<Route path="/visualizations" component={Visualizations} />

				<Route exact path="/search/" component={GlobalSearchPage} />
				<Route path="/search/:query" component={GlobalSearchPage} />
				<Route component={ErrorPage} />
			</Switch>
		</main>
	)
}

export default App
