import React from "react"
import { Card, Image } from "react-bootstrap"
import Highlight from "react-highlighter"

function AlbumSearchCard(props) {
	return (
		<div className="text-left">
			<Card className="mx-auto" style={{ width: "32rem" }}>
				<a href={"https://spotifynder.com/album/" + props.album.albumID}>
					<div className="container">
						<div className="row">
							<div className="col-md-6">
								<Card.Body className="text-dark">
									<a href={"https://spotifynder.com/album/" + props.album.albumID}>
										<Card.Title className="text-">
											<Highlight search={props.search}>{props.album.name}</Highlight>
										</Card.Title>
									</a>
									<Card.Text>
										Artist: <Highlight search={props.search}>{props.album.artist}</Highlight>
									</Card.Text>
									<Card.Text>
										Genre: <Highlight search={props.search}>{props.album.genres === "" ? "Unknown" : props.album.genres}</Highlight>
									</Card.Text>
									<Card.Text>
										Top Track: <Highlight search={props.search}>{props.album.recommendedSong}</Highlight>
									</Card.Text>
									<Card.Text>
										Studio: <Highlight search={props.search}>{props.album.label}</Highlight>
									</Card.Text>
								</Card.Body>
							</div>
							<div className="col-md-6">
								<Image src={props.album.albumImage} alt={props.album.name + " Image"} fluid thumbnail />
							</div>
						</div>
					</div>
				</a>
			</Card>
		</div>
	)
}

export default AlbumSearchCard
