import React from "react"
import { Card, Image } from "react-bootstrap"
import grey from "../Images/grey.png"
import Highlight from "react-highlighter"

function ArtistSearchCard(props) {
	return (
		<div className="text-left">
			<Card className="mx-auto" style={{ width: "32rem" }}>
				<a href={"https://spotifynder.com/artist/" + props.artist.artistID}>
					<div className="container">
						<div className="row d-flex flex-wrap align-items-center">
							<div className="col-md-6">
								<Card.Body className="text-dark">
									<a href={"https://spotifynder.com/artist/" + props.artist.artistID}>
										<Card.Title className="text-">
											<Highlight search={props.search}>{props.artist.name}</Highlight>
										</Card.Title>
									</a>
									<Card.Text>
										Genre: <Highlight search={props.search}>{props.artist.strGenre}</Highlight>
									</Card.Text>
									<Card.Text>
										Popularity: {props.artist.popularity}
									</Card.Text>
									<Card.Text>
										Active Years: {props.artist.intFormedYear}-{props.artist.strDisbanded}
									</Card.Text>
									<Card.Text>
										From: <Highlight search={props.search}>{props.artist.strCountry}</Highlight>
									</Card.Text>
								</Card.Body>
							</div>
							<div className="col-md-6">
								{props.artist.strArtistThumb !== "" ? 
									<Image src={props.artist.strArtistThumb} alt={props.artist.name + " Image"} fluid thumbnail /> 
									: 
									<Image src={grey} alt={"grey"} fluid thumbnail />}
							</div>
						</div>
					</div>
				</a>
			</Card>
		</div>
	)
}

export default ArtistSearchCard
