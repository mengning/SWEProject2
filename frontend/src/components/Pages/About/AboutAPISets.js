import React from "react"
import Card from "react-bootstrap/Card"

function AboutAPISets() {
	const sets = [
		{
			href: "https://developer.spotify.com/documentation/web-api/",
			caption: "Spotify API",
			purpose: "Used to generate information on songs, artists, and albums.",
			src: "https://cdn2.iconfinder.com/data/icons/font-awesome/1792/spotify-512.png",
			key: 0,
		},
		{
			href: "https://www.theaudiodb.com/api_guide.php",
			caption: "AudioDB API",
			purpose: "Used to fill in supplementary information on artists.",
			src: "https://i1.sndcdn.com/avatars-000166546285-0ty1ex-t500x500.jpg",
			key: 1,
		},
		{
			href: "https://www.last.fm/api",
			caption: "Last.FM API",
			purpose: "Used to fill in supplementary information on albums",
			src: "https://www.last.fm/static/images/lastfm_avatar_twitter.52a5d69a85ac.png",
			key: 2,
		},
	]

	function renderSlides() {
		return sets.map((set) => (
			<div key={set.key} className="col-md-4">
				<a href={set.href} className="text-dark">
					<Card className="text-center text-dark">
						<Card.Img variant="bottom" fluid width="200 px" src={set.src} />
						<Card.Footer>
							<u className="text-primary">{set.caption}</u>
							<br />
							{set.purpose}
						</Card.Footer>
					</Card>
				</a>
			</div>
		))
	}

	return (
		<div className="text-center">
			<h2>APIs and Data Sources</h2>
			<p>
                Raw csv data formatted using Jupyter Notebook and Pandas. Flask, SQL Alchemy, and URL Lib were used to get data from the external APIs and create the JSON response.
            </p>
			<br />
			<div className="container">
				<div className="row justify-content-around">{renderSlides()}</div>
			</div>
		</div>
	)
}

export default AboutAPISets
