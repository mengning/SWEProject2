import React, { useState, useEffect } from "react"
import { Link, useParams } from "react-router-dom"
import NavBar from "../../NavBar"
const https = require("https")
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

function AlbumInstance() {
	const { albumID } = useParams()
	const [albumData, setAlbumData] = useState({})

	const getAlbumData = async () => {
		try {
			const httpsAgent = new https.Agent({
				rejectUnauthorized: false,
			})
			const requestOptions = {
				method: "GET",
				agent: httpsAgent,
				headers: {},
			}
			const response = await fetch(`https://spotifynder.com/api/albums?albumID=${albumID}`, requestOptions)
			const data = await response.json()
			setAlbumData(data.message[0])
			console.log(data.message)
		} catch (e) {
			console.error(e)
		}
	}

	useEffect(() => {
		getAlbumData()
	})

	return (
		<div>
			<div>
				<NavBar />
			</div>
			<a href="/Albums" class="btn btn-primary" style={{ marginTop: "15px", marginLeft: "15px" }}>
				Back to Albums
			</a>
			<div style={{ margin: "auto", marginTop: "50px", width: "80%" }}>
				<div class="jumbotron">
					<div>
						<h3 class="display-3" style={{ display: "inline-block", width: "75%" }}>
							{albumData.name}
						</h3>
						<img src={albumData.albumImage} alt={albumData.name} style={{ float: "right", width: "250px" }} />
						<h1 class="display-4" style={{ marginBottom: "150px" }}>
							Artist:{" "}
							<Link key={albumID} to={`/artist/${albumData.artistID}`}>
								{albumData.artist}
							</Link>
						</h1>
					</div>
					<hr class="my-4"></hr>
					<div id="body">
						<div style={{ display: "inline-block" }}>
							<h3>
								<strong>Genre</strong>: {albumData.genres === "" ? "Unknown" : albumData.genres}
							</h3>
							<h3>
								<strong>Released</strong>: {albumData.releaseDate}
							</h3>
							<h3>
								<strong>Tracks</strong>: {albumData.totalTracks}
							</h3>
							<h3>
								<strong>Studio</strong>: {albumData.label}
							</h3>
							<h3>
								<strong>Recommended Song</strong>:{" "}
								<Link key={albumID} to={`/song/${albumData.recommendedSongID}`}>
									{albumData.recommendedSong}
								</Link>
							</h3>
						</div>
						<iframe title="Album preview" src={`https://open.spotify.com/embed/album/${albumData.albumID}`} width="250" height="250" frameborder="0" allowtransparency="true" allow="encrypted-media" style={{ float: "right" }}></iframe>
					</div>
				</div>
			</div>
		</div>
	)
}

export default AlbumInstance
