import React, { useState, useEffect, Fragment, useRef } from "react"
import { useParams } from "react-router-dom"
import { Input } from "antd"
import "antd/dist/antd.css"
import NavBar from "../NavBar"
import SongSearchCard from "../Cards/SongSearchCard"
import ArtistSearchCard from "../Cards/ArtistSearchCard"
import AlbumSearchCard from "../Cards/AlbumSearchCard"
const https = require("https")
const { Search } = Input
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

function GlobalSearchPage() {
	const { query } = useParams()

	useEffect(() => {
		console.log(query)
		if (query !== undefined) {
			setSearch(query)
		}
	}, [query])

	const [loading, setLoading] = useState(true)

	const [songData, setSongData] = useState([])
	const [artistData, setArtistData] = useState([])
	const [albumData, setAlbumData] = useState([])

	const [search, setSearch] = useState(query)

	const songRef = useRef(null)
	const artistRef = useRef(null)
	const albumRef = useRef(null)

	//Songs
	useEffect(() => {
		//Make API call with parameters for current page and per page to get the data for this page

		const fetchSongs = async () => {
			try {
				setLoading(true)
				const httpsAgent = new https.Agent({
					rejectUnauthorized: false,
				})
				const requestOptions = {
					method: "GET",
					agent: httpsAgent,
					headers: {},
				}

				var response
				if (search === "") response = await fetch(`https://spotifynder.com/api/songs`, requestOptions)
				else response = await fetch(`https://spotifynder.com/api/songs?search=${search}`, requestOptions)

				const data = await response.json()

				if (data.message.length > 12) {
					setSongData(data.message.slice(0, 12))
				} else {
					setSongData(data.message)
				}
				console.log("Fetched songs")

				setLoading(false)
			} catch (error) {
				console.error(error)
			}
		}
		fetchSongs()
	}, [search])

	//Artists
	useEffect(() => {
		//Make API call with parameters for current page and per page to get the data for this page

		const fetchArtists = async () => {
			try {
				setLoading(true)
				const httpsAgent = new https.Agent({
					rejectUnauthorized: false,
				})
				const requestOptions = {
					method: "GET",
					agent: httpsAgent,
					headers: {},
				}

				var response
				if (search === "") response = await fetch(`https://spotifynder.com/api/artists`, requestOptions)
				else response = await fetch(`https://spotifynder.com/api/artists?search=${search}`, requestOptions)
				const data = await response.json()

				if (data.message.length > 12) {
					setArtistData(data.message.slice(0, 12))
				} else {
					setArtistData(data.message)
				}
				console.log("Fetched artists")

				setLoading(false)
			} catch (error) {
				console.error(error)
			}
		}
		fetchArtists()
	}, [search])

	//Albums
	useEffect(() => {
		//Make API call with parameters for current page and per page to get the data for this page

		const fetchAlbums = async () => {
			try {
				setLoading(true)
				const httpsAgent = new https.Agent({
					rejectUnauthorized: false,
				})
				const requestOptions = {
					method: "GET",
					agent: httpsAgent,
					headers: {},
				}

				var response
				if (search === "") response = await fetch(`https://spotifynder.com/api/albums`, requestOptions)
				else response = await fetch(`https://spotifynder.com/api/albums?search=${search}`, requestOptions)
				const data = await response.json()

				if (data.message.length > 12) {
					setAlbumData(data.message.slice(0, 12))
				} else {
					setAlbumData(data.message)
				}
				console.log("Fetched albums")

				setLoading(false)
			} catch (error) {
				console.error(error)
			}
		}
		fetchAlbums()
	}, [search])

	return (
		<Fragment>
			<NavBar />

			<div
				style={{
					margin: "auto",
					justifyContent: "center",
					width: "80%",
				}}>
				<h1
					style={{
						marginTop: "25px",
						textAlign: "center",
					}}>
					Search
				</h1>
				<div class="search-bar" style={{ display: "flex" }}>
					<Search placeholder="search.." onSearch={setSearch} defaultValue={search}/>
				</div>
				<br />
				<div>
					<h3>Song Results:</h3>
				</div>

				<div>
					{!loading ? (
						<div
							style={{
								margin: "auto",
								marginTop: 50,
							}}>
							<section
								style={{
									display: "grid",
									gridTemplateRows: "repeat(auto-fill, minmax(300px, 1fr))",
									gridTemplateColumns: "repeat(auto-fill, minmax(400px, 1fr))",
									gap: "50px",
								}}
								ref={songRef}>
								{songData?.map((data) => (
									<SongSearchCard song={data} search={search} />
								))}
							</section>
						</div>
					) : (
						<div>Loading...</div>
					)}
				</div>
				<br />
				<div>
					<h4>
						<a href="/songs/search">Couldn't Find The Song You Wanted?</a>
					</h4>
				</div>
				<br />
				<div>
					<h3>Artist Results:</h3>
				</div>

				{!loading ? (
					<div
						style={{
							margin: "auto",
							marginTop: 50,
						}}>
						<section
							style={{
								display: "grid",
								gridTemplateRows: "repeat(auto-fill, minmax(300px, 1fr))",
								gridTemplateColumns: "repeat(auto-fill, minmax(400px, 1fr))",
								gap: "50px",
							}}
							ref={artistRef}>
							{artistData?.map((data) => (
								<ArtistSearchCard artist={data} search={search} />
							))}
						</section>
					</div>
				) : (
					<div>Loading...</div>
				)}

				<div>
					<h4>
						<a href="/artists/search">Couldn't Find The Artist You Wanted?</a>
					</h4>
				</div>

				<div>
					<h3>Album Results:</h3>
				</div>

				{!loading ? (
					<div
						style={{
							margin: "auto",
							marginTop: 50,
						}}>
						<section
							style={{
								display: "grid",
								gridTemplateRows: "repeat(auto-fill, minmax(300px, 1fr))",
								gridTemplateColumns: "repeat(auto-fill, minmax(400px, 1fr))",
								gap: "50px",
							}}
							ref={albumRef}>
							{albumData?.map((data) => (
								<AlbumSearchCard album={data} search={search} />
							))}
						</section>
					</div>
				) : (
					<div>Loading...</div>
				)}

				<div>
					<h4>
						<a href="/albums">Couldn't Find The Album You Wanted?</a>
					</h4>
				</div>
			</div>
		</Fragment>
	)
}

export default GlobalSearchPage
