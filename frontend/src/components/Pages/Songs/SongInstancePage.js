import React, { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import NavBar from "../../NavBar"
import Image from "react-bootstrap/Image"
const https = require("https")
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

function SongInstancePage() {
	const { songID } = useParams()
	const [songData, setSongData] = useState({})

	const getSongData = async () => {
		try {
			const httpsAgent = new https.Agent({
				rejectUnauthorized: false,
			})
			const requestOptions = {
				method: "GET",
				agent: httpsAgent,
				headers: {},
			}
			const response = await fetch(`https://spotifynder.com/api/songs?songID=${songID}`, requestOptions)
			const data = await response.json()
			setSongData(data.message[0])
		} catch (e) {
			console.error(e)
		}
	}

	useEffect(() => {
		getSongData()
	})

	return (
		<div>
			<div>
				<NavBar />
			</div>

			<a href="/songs" class="btn btn-primary" style={{ marginTop: "15px", marginLeft: "15px" }}>
				Back to Songs
			</a>
			<div style={{ margin: "auto", marginTop: "50px", width: "80%" }}>
				<div class="jumbotron">
					<h1 class="display-1" style={{ display: "inline-block", width: "70%" }}>
						{songData.name}
					</h1>
					<Image src={songData.image} alt={songData.album} fluid style={{ float: "right", width: 300 }} />
					<h1 class="display-4" style={{ width: "70%" }}>
						Artist:{" "}
						<a href={"/artist/" + songData.artistID} id="track-artist">
							{songData.artist}
						</a>
					</h1>
					<h1 class="display-4" style={{ width: "70%" }}>
						Album:{" "}
						<a href={"/album/" + songData.albumID} id="track-album">
							{songData.album}
						</a>
					</h1>
					<hr class="my-4"></hr>
					<h3 style={{ display: "inline-block" }}>Genre: {songData.genres}</h3>
					<iframe title="Song preview" src={"https://open.spotify.com/embed/track/" + songData.songID} width="250" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media" style={{ float: "right" }}></iframe>
					<h3>Acousticness: {round(songData.acousticness * 100)}%</h3>
					<h3>Danceability: {round(songData.danceability * 100)}%</h3>
					<h3>Energy: {round(songData.energy * 100)}%</h3>
					<h3>Liveness: {round(songData.liveness * 100)}%</h3>
					<h3>Instrumental: {songData.instrumentalness > 0.5 ? "Yes" : "No"}</h3>
				</div>
			</div>
		</div>
	)
}

/**
 * Rounds a number to one decimal place.
 * @param {A string to round} num
 * @returns the rounded number.
 */
function round(num) {
	return parseFloat(num).toFixed(1)
}

export default SongInstancePage
