import React, { useState, useEffect, Fragment, useRef } from "react"
import "./Songs.css"
import { Pagination, Input, Select, Slider } from "antd"
import "antd/dist/antd.css"
import NavBar from "../../NavBar"
import Filter from "../../Filter"
import SongSearchCard from "../../Cards/SongSearchCard"
const https = require("https")
const { Search } = Input
const { Option } = Select
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

function SongSearchPage() {
	const [loading, setLoading] = useState(true)
	const [songData, setSongData] = useState([])
	const [total, setTotal] = useState(20)
	const [page, setPage] = useState(1)
	const [perPage, setPerPage] = useState(20)
	const [search, setSearch] = useState("")
	const [artist, setArtist] = useState([])
	const [genres, setGenres] = useState([])
	const [popularity, setPopularity] = useState([])
	const [duration, setDuration] = useState([])
	const [sortBy, setSortBy] = useState("name")
	const [sortDir, setSortDir] = useState(0)
	const [onPage, setOnPage] = useState(12)
	const songRef = useRef(null)

	const pageChange = (page, perPage) => {
		setPage(page)
		setPerPage(perPage)
		console.log(page)
		console.log(perPage)
	}

	function secToMinSec(seconds) {
		var minutes = Math.floor(seconds / 60)
		return `${minutes}:${seconds % 60}${seconds < 10 ? "0" : ""}`
	}

	useEffect(() => {
		const fetchSongs = async () => {
			try {
				setLoading(true)
				const httpsAgent = new https.Agent({
					rejectUnauthorized: false,
				})
				const requestOptions = {
					method: "GET",
					agent: httpsAgent,
					headers: {},
				}

				var link = `https://spotifynder.com/api/songs?`
				link += "page="
				link += page
				link += "&perPage="
				link += perPage
				if (search !== "" && search !== undefined) link += "&search="
				link += search
				if (artist.length !== 0) {
					artist.forEach(function (a) {
						link += "&artist="
						link += a
					})
				}
				if (genres.length !== 0) {
					genres.forEach(function (g) {
						link += "&genres="
						link += g
					})
				}
				if (popularity.length !== 0) {
					popularity.forEach(function (a) {
						link += "&popularity="
						link += a
					})
				}
				if (duration.length !== 0) {
					duration.forEach(function (a) {
						link += "&duration_ms="
						link += a * 1000
					})
				}
				link += "&sort="
				link += sortBy
				link += "&direction="
				link += sortDir
				const response = await fetch(link, requestOptions)
				const data = await response.json()
				console.log("fetched")
				setSongData(data.message)
				setTotal(data.count)
				setOnPage(perPage)
				if (page - 1 === total / perPage) {
					setOnPage(total % perPage)
				}

				setLoading(false)
			} catch (error) {
				console.error(error)
			}
		}
		fetchSongs()
	}, [page, perPage, search, artist, genres, popularity, duration, sortBy, sortDir, total])

	return (
		<Fragment>
			<NavBar />

			<h1 style={{ marginTop: "25px", textAlign: "center" }}>Search for a specific song.</h1>

			<div
				class="search-bar"
				style={{
					display: "flex",
					margin: "auto",
					justifyContent: "center",
					width: "85%",
				}}>
				<Search addonBefore="Search" placeholder="search.." onSearch={setSearch} />
			</div>

			<div class="filter-bar">
				<Filter name={"Artist"} onChange={setArtist} style={{ width: "100%" }}></Filter>
				<Filter name={"Genre"} onChange={setGenres} style={{ width: "100%" }}></Filter>

				<Select onSelect={setSortBy} defaultValue="name" style={{ width: "10%" }}>
					<Option value="name">Name</Option>
					<Option value="artist">Artist</Option>
					<Option value="genres">Genres</Option>
					<Option value="popularity">Popularity</Option>
					<Option value="duration_ms">Song Duration</Option>
				</Select>
				<Select onSelect={setSortDir} defaultValue="0" style={{ width: "10%" }}>
					<Option value="0">Ascending</Option>
					<Option value="1">Descending</Option>
				</Select>
			</div>

			<div class="filter-bar">
				<div class="filter-box-2">
					<span style={{ display: "inline-block" }}>Popularity</span>
					<Slider range defaultValue={[0, 100]} onAfterChange={setPopularity} />
				</div>
				<div class="filter-box-2">
					<span style={{ display: "inline-block" }}>Song Duration</span>
					<Slider tipFormatter={secToMinSec} range defaultValue={[0, 1300]} max="1300" onAfterChange={setDuration} />
				</div>
			</div>

			{!loading ? (
				<div style={{ width: "85%", margin: "auto", marginTop: 50 }}>
					<section
						style={{
							display: "grid",
							gridTemplateRows: "repeat(auto-fill, minmax(300px, 1fr))",
							gridTemplateColumns: "repeat(auto-fill, minmax(400px, 1fr))",
							gap: "50px",
						}}
						ref={songRef}>
						{songData?.map((data) => (
							<SongSearchCard song={data} search={search} key={data.id} />
						))}
					</section>
				</div>
			) : (
				<div>Loading...</div>
			)}
			<Pagination
				total={total}
				pageSize={perPage}
				onChange={pageChange}
				defaultCurrent={1}
				style={{
					margin: "16px 0",
					display: "flex",
					justifyContent: "center",
				}}
				showTotal={(total) => `Showing ${onPage} of ${total} items`}
			/>
		</Fragment>
	)
}

export default SongSearchPage
