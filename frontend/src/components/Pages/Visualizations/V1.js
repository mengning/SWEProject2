import React, { useState, useEffect, Fragment, useRef, Component, PureComponent} from "react";
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';

const toPercent = (decimal, fixed = 0) => `${(decimal * 100)}%`;

const getPercent = (value, total) => {
    const ratio = total > 0 ? value / total : 0;    
    return toPercent(ratio, 2);
};  

const renderTooltipContent = (o) => {
    const { payload, label } = o;
    const total = payload.reduce((result, entry) => result + entry.value, 0);
  
    return (
      <div className="customized-tooltip-content">
        <p className="total">{`${label} (Total: ${total})`}</p>
        <ul className="list">
          {payload.map((entry, index) => (
            <li key={`item-${index}`} style={{ color: entry.color }}>
              {`${entry.name}: ${entry.value}(${getPercent(entry.value, total)})`}
            </li>
          ))}
        </ul>
      </div>
    );
  };

class V1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    async componentDidMount() {
        const requestOptions = {
                method: "GET",
                headers: {},
            }

            var link = `https://spotifynder.com/api/artists`
            const response = await fetch(link, requestOptions)
            const data = await response.json()
        this.setState({
            data: data.message
        })
    }

    getValues() {
        var values = []
        
        for(var year = 1960; year < 2020; year += 10) {
            let dict = {
                decade: year,
                pop: 0,
                rock: 0,
                country: 0,
                indie: 0,
                hiphop: 0,
                rb: 0,
                electro: 0
            }
            for(let artist of this.state.data){
                if(artist.intFormedYear != "Unknown" && artist.intFormedYear >= year && artist.intFormedYear <= year+10)
                {
                    if(artist.strGenre.includes("Indie") || artist.strGenre.includes("New") || artist.strGenre.includes("Experimental") )
                    {
                        dict.indie++
                    }
                    else if(artist.strGenre.includes("Rock") || artist.strGenre.includes("Metal"))
                    {
                        dict.rock++
                    }
                    else if(artist.strGenre.includes("Country") || artist.strGenre.includes("BlueGrass") || artist.strGenre.includes("Folk") || artist.strGenre.includes("Soul") || artist.strGenre.includes("Christian") )
                    {
                        dict.country++
                    }
                    else if(artist.strGenre.includes("Pop"))
                    {
                        dict.pop++
                    }
                    else if(artist.strGenre.includes("Hip") || artist.strGenre.includes("Rap") || artist.strGenre.includes("Hop"))
                    {
                        dict.hiphop++
                    }
                    else if(artist.strGenre.includes("R&B") || artist.strGenre.includes("R&b") || artist.strGenre.includes("Jazz"))
                    {
                        dict.rb++
                    }
                    else if(artist.strGenre.includes("Electro") || artist.strGenre.includes("Synth") || artist.strGenre.includes("Trap") || artist.strGenre.includes("House"))
                    {
                        dict.electro++ 
                    }
                }
            }
            values.push(dict)
        }

        return values;
    }

    render() {
        let data = this.state.data;

        return (data != null) ? (
            <div>
                <h3 className="text-center" style={{ marginTop: '30px'}}> Frequency of Artist Genre by Decade</h3>
                <div style={{ display: "flex", alignItems: "center", justifyContent: "center"}} >
                    <AreaChart
                        width={750}
                        height={600}
                        data={this.getValues()}
                        stackOffset="expand"
                        margin={{
                            top: 10,
                            right: 0,
                            left: 0,
                            bottom: 0,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="decade" />
                        <YAxis tickFormatter={toPercent} />
                        <Tooltip content={renderTooltipContent} />
                        <Area type="monotone" dataKey="pop" stackId="1" stroke="#8884d8" fill="#8884d8" />
                        <Area type="monotone" dataKey="rock" stackId="1" stroke="#82ca9d" fill="#82ca9d" />
                        <Area type="monotone" dataKey="country" stackId="1" stroke="#ffc658" fill="#ffc658" />
                        <Area type="monotone" dataKey="indie" stackId="1" stroke="#09b913" fill="#09b913" />
                        <Area type="monotone" dataKey="hiphop" stackId="1" stroke="#e86c8f" fill="#e86c8f" />
                        <Area type="monotone" dataKey="rb" stackId="1" stroke="#cf683f" fill="#cf683f" />
                        <Area type="monotone" dataKey="electro" stackId="1" stroke="#1fa78e" fill="#1fa78e" />
                    </AreaChart>
                </div>
            </div>
        ) : (
            <div>
                <h3 className="text-center"> Frequency of Artist Genre by Decade</h3>
                <p className="text-center"> Loading Data...</p>
            </div>
        );
    }
}

export default V1;
