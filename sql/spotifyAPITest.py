# LINK TO TUTORIAL I FOLLOWED: https://www.youtube.com/watch?v=9mKAAWRheTA
import requests, base64, json, csv
from secrets import *

# step1: get authentication token
# step2: access the api using said token

# curl -X "POST" -H "Authorization: Basic ZjM4ZjAw...WY0MzE=" -d grant_type=client_credentials https://accounts.spotify.com/api/token
def getSpotifyAccessToken(clientID, clientSecret):
    authURL = "https://accounts.spotify.com/api/token"
    authHeader = {}
    authData = {}

    # base64 encode client id and secret
    message = f"{clientID}:{clientSecret}"
    messageBytes = message.encode('ascii')
    base64Bytes = base64.b64encode(messageBytes)
    base64Message = base64Bytes.decode('ascii')
    # print(base64Message)

    # sending the post request to get our authentication token
    authHeader['Authorization'] = "Basic " + base64Message
    authData['grant_type'] = "client_credentials"
    result = requests.post(authURL, headers=authHeader, data=authData)
    # print(result)

    responseObject = result.json()
    # print(json.dumps(responseObject, indent=2))

    accessToken = responseObject['access_token']
    return accessToken


def get_genre(artistName, artistID):
    artistName = artistName.replace(" ", "_")
    audioDBURL = f"https://www.theaudiodb.com/api/v1/json/1/search.php?s={artistName}"
    artistResult = requests.get(audioDBURL).json()['artists']

    if (artistResult != None):
        return artistResult[0]['strGenre']
    else:
        accessToken = getSpotifyAccessToken(clientID, clientSecret)
        header = {"Authorization": "Bearer " + accessToken}
        getAnArtistURL = f"https://api.spotify.com/v1/artists/{artistID}"
        artistResult = requests.get(getAnArtistURL, headers=header).json()
        if (len(artistResult['genres']) == 0):
            return "Unknown"
        else:
            return artistResult['genres'][0].title()


# ------------------------------------------------------------------------------------------
# Songs: Name, Artist, Album, Popularity, Danceability
# How You Like That, BLACKPINK, ?, ?, ?, ?
# getTrackURL = f"https://api.spotify.com/v1/tracks/{id}"
def get_song_info(songID):
    accessToken = getSpotifyAccessToken(clientID, clientSecret)
    header = {"Authorization": "Bearer " + accessToken}
    songInfo = {}

    getTrackURL = f"https://api.spotify.com/v1/tracks/{songID}"
    songResult = requests.get(getTrackURL, headers=header).json()
    songInfo['name'] = (songResult['name'])
    songInfo['nameID'] = (songResult['id'])
    songInfo['artist'] = (songResult['artists'][0]['name'])
    songInfo['artistID'] = (songResult['artists'][0]['id'])
    songInfo['album'] = (songResult['album']['name'])
    songInfo['albumID'] = (songResult['album']['id'])

    songInfo['genres'] = get_genre(songInfo['artist'], songInfo['artistID'])

    # getAnArtistURL = f"https://api.spotify.com/v1/artists/{songResult['artists'][0]['id']}"
    # artistResult = requests.get(getAnArtistURL, headers=header).json()
    # songInfo['genres'] = (artistResult['genres'])

    songInfo['popularity'] = (songResult['popularity'])

    getTunableURL = f"https://api.spotify.com/v1/audio-features/{songID}"
    tunableResult = requests.get(getTunableURL, headers=header).json()
    songInfo['duration_ms'] = (tunableResult['duration_ms'])
    songInfo['acousticness'] = (tunableResult['acousticness'])
    songInfo['danceability'] = (tunableResult['danceability'])
    songInfo['energy'] = (tunableResult['energy'])
    songInfo['liveness'] = (tunableResult['liveness'])
    songInfo['instrumentalness'] = (tunableResult['instrumentalness'])

    # preview url is: https://open.spotify.com/embed/track/{trackID}

    # album picture
    songInfo['image'] = (songResult['album']['images'][0]['url'])
    return songInfo


# Artists: name, followers, popularity, top track, genre, yearFormed, yearDisbanded, country, members, thumbnail, twitter link
# http://developers.music-story.com/developers/artist
def get_artist_info(artistID):
    accessToken = getSpotifyAccessToken(clientID, clientSecret)
    header = {"Authorization": "Bearer " + accessToken}
    artistInfo = {}

    getAnArtistURL = f"https://api.spotify.com/v1/artists/{artistID}"
    artistResult = requests.get(getAnArtistURL, headers=header).json()
    artistInfo['name'] = artistResult['name']
    artistInfo['artistID'] = artistResult['id']
    # artistInfo.append(artistResult['genres'])
    artistInfo['followers'] = artistResult['followers']['total']
    artistInfo['popularity'] = artistResult['popularity']
    getAnArtistTopTracks = f"https://api.spotify.com/v1/artists/{artistID}/top-tracks"
    reqData = {"market": "US"}
    topTracksResult = requests.get(getAnArtistTopTracks, headers=header, params=reqData).json()

    try:
        artistInfo['topTrack'] = topTracksResult['tracks'][0]['name']
        artistInfo['topID'] = topTracksResult['tracks'][0]['id']
        getTopSongURL = f"https://api.spotify.com/v1/tracks/{artistInfo['topID']}"
        songResult = requests.get(getTopSongURL, headers=header).json()
        artistInfo['topAlbum'] = songResult['album']['name']
        artistInfo['topAlbumID'] = songResult['album']['id']
    except:
        artistInfo['topTrack'] = "Unknown"
        artistInfo['topID'] = ""
        artistInfo['topAlbum'] = "Unknown"
        artistInfo['topAlbumID'] = ""

    artistName = artistResult['name'].replace(" ", "_")
    audioDBURL = f"https://www.theaudiodb.com/api/v1/json/1/search.php?s={artistName}"
    artistResult = requests.get(audioDBURL).json()['artists']
    if (artistResult == None):
        artistInfo['intFormedYear'] = -1
        artistInfo['strDisbanded'] = "Unknown"
        artistInfo['strCountry'] = "Unknown"
        artistInfo['intMembers'] = -1
        artistInfo['strArtistThumb'] = ""
        artistInfo['strTwitter'] = ""
    else:
        artistResult = artistResult[0]
        artistInfo['intFormedYear'] = artistResult['intFormedYear']
        artistInfo['strDisbanded'] = (
        artistResult['strDisbanded'] if artistResult['strDisbanded'] != None else 'Present')
        artistInfo['strCountry'] = artistResult['strCountry']
        artistInfo['intMembers'] = artistResult['intMembers']
        artistInfo['strArtistThumb'] = artistResult['strArtistThumb']
        artistInfo['strTwitter'] = artistResult['strTwitter']

    artistInfo['strGenre'] = get_genre(artistInfo['name'], artistInfo['artistID'])
    
    return artistInfo



# blackpink: 41MozSoPIsD1dJM0CLPjZF
# brent walsh: 5BKIMooS05pnIAEd39a7Wo
# smash mouth: 2iEvnFsWxR0Syqu2JNopAd

# https://developer.spotify.com/documentation/web-api/reference/#category-albums
# https://api.spotify.com/v1/albums/{id}
# Albums: name, artist, release_date, num_tracks, popularity
def get_album_info(albumID):
    accessToken = getSpotifyAccessToken(clientID, clientSecret)
    header = {"Authorization": "Bearer " + accessToken}
    albumInfo = {}

    getAnAlbumURL = f"https://api.spotify.com/v1/albums/{albumID}"
    albumprint = requests.get(getAnAlbumURL, headers=header)
    albumResult = albumprint.json()
    
    # artistName = albumResult['artists'][0]['name']
    # artistName = artistName.replace(" ", "_")
    # audioDBURL = f"https://www.theaudiodb.com/api/v1/json/1/search.php?s={artistName}"
    # artistResult = requests.get(audioDBURL).json()['artists']
    # if(artistResult == None or artistResult['strGenre'] == "[]"):
    #     albumInfo['genres'] = "Unknown"
    # else:
    #     albumInfo['genres'] = artistResult['strGenre']

    try:
        albumInfo['name'] = albumResult['name']
        albumInfo['albumID'] = albumResult['id']
        albumInfo['artist'] = albumResult['artists'][0]['name']
        albumInfo['artistID'] = albumResult['artists'][0]['id']
        albumInfo['popularity'] = albumResult['popularity']
        albumInfo['totalTracks'] = albumResult['total_tracks']
        albumInfo['releaseDate'] = albumResult['release_date']
        albumInfo['label'] = albumResult['label']
        albumInfo['genres'] = get_genre(albumInfo['artist'], albumInfo['artistID'])
    except:
        print(albumprint)
        

    # preview url is: https://open.spotify.com/embed/album/{albumID}

    albumInfo['albumImage'] = albumResult['images'][0]['url']
    # songs in the album (do in sql)

    return albumInfo


# ------------------------------------------------------------------------------------------------------------------------------
def write_csv(info, fieldnames, csvName=None):
    with open(csvName, 'w') as csvFile:
        writer = csv.DictWriter(csvFile, fieldnames=fieldnames)
        for i in range(len(info)):
            writer.writerow(info[i])


songFieldNames = ['name', 'nameID', 'artist', 'artistID', 'album', 'albumID', 'genres', 'popularity', 'duration_ms',
                  'acousticness', 'danceability', 'energy', 'liveness', 'instrumentalness', 'image']
artistFieldNames = ['name', 'artistID', 'followers', 'popularity', 'topTrack', 'topID', 'topAlbum', 'topAlbumID',
                    'strGenre', 'intFormedYear', 'strDisbanded', 'strCountry', 'intMembers', 'strArtistThumb',
                    'strTwitter']
albumFieldNames = ['name', 'albumID', 'artist', 'artistID', 'genres', 'popularity', 'totalTracks', 'releaseDate',
                   'label', 'albumImage']

def get_everything():
    accessToken = getSpotifyAccessToken(clientID, clientSecret)
    header = {"Authorization": "Bearer " + accessToken}

    songs = []
    artists = []
    albums = []

    for year in range(2021, 1999, -1):
        print("YEAR:" + str(year))
        getSongsURL = f"https://api.spotify.com/v1/search?q=year%3A{year}&type=track&limit=50&offset={0}"
        try:
            songsResult = requests.get(getSongsURL, headers=header).json()["tracks"]["items"]
            for t in songsResult:
                # all my homies HATE Armin van Buuren
                if t["artists"][0]["id"] != "0SfsnGyD8FpIN4U4WCkBZ5": 
                    print(get_artist_info(t["artists"][0]["id"])["name"])
                    try:
                        song = get_song_info(t["id"])
                        artist = get_artist_info(t["artists"][0]["id"])
                        album = get_album_info(t["album"]["id"])

                        #if artist's top track is not this song, get and add it
                        if(artist["topID"] != song["nameID"]) :
                            topTrack = get_song_info(artist["topID"])
                            songs.append(topTrack)
                            #if the top track is not in the same album as this song, get and add it
                            if (song["albumID"] != topTrack["albumID"]) :
                                topAlbum = get_album_info(topTrack["albumID"])
                                albums.append(topAlbum)

                        songs.append(song)
                        artists.append(artist)
                        albums.append(album)
                    except:
                        print("Failed Single Song")
                        #give up and be sad
        except:
            print("Failed Year")

    return songs, artists, albums

if __name__ == "__main__":
    # songs, artists, albums = get_everything()

    # write_csv(songs, songFieldNames, "tempSongs.csv")

    # write_csv(artists, artistFieldNames, "tempArtists.csv")

    # write_csv(albums, albumFieldNames, "tempAlbums.csv")
    artist = get_artist_info("790FomKkXshlbRYZFtlgla")
    fields = ""
    for n in artist:
        fields = fields + str(artist[n]) + ", "
    print(fields)
